@file:JvmName("DataStoreConverter")

package com.example.gcp.demo

import com.google.cloud.Timestamp
import com.google.cloud.datastore.*

private fun convertToEntity(
        message: Message,
        keyFactory: (String) -> KeyFactory
): FullEntity<IncompleteKey> {

    val rowKeyFactory: KeyFactory = keyFactory("Messages")

    return FullEntity.newBuilder(rowKeyFactory.newKey())
            .set("datetime", Timestamp.now())
            .set("message", message.text)
            .build()
}

fun saveToDataStore(
        msg: Message
) {
    val datastore: Datastore = DatastoreOptions.getDefaultInstance().service

    val keyFactoryBuilder = { s: String ->
        datastore.newKeyFactory().setKind(s)
    }

    val entity = convertToEntity(msg, keyFactoryBuilder)

    datastore.add(entity)
}

fun getFromDataStore(): List<Message> {
    val datastore: Datastore = DatastoreOptions.getDefaultInstance().service
    val query: Query<Entity> = Query.newEntityQueryBuilder()
            .setKind("Messages")
            .setOrderBy(StructuredQuery.OrderBy.desc("datetime"))
            .build()

    val messages: QueryResults<Entity> = datastore.run(query)

    return messages.asSequence().map { entity -> convertToMessage(entity) }.toList()
}

fun convertToMessage(
        entity: Entity
): Message {
    val text = entity.getString("message")
    return Message(text)
}