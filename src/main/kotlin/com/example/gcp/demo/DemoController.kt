@file:JvmName("DemoController")

package com.example.gcp.demo

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController


@RestController
class DemoController {
    @GetMapping(value = ["/messages"])
    fun getMessages(): List<Message> = getFromDataStore()

    @PostMapping(value = ["/messages"])
    fun addMessage(@RequestBody msg: Message): ResponseEntity<Any> {
        saveToDataStore(msg)
        return ResponseEntity
                .ok()
                .build()
    }

    @GetMapping
    fun root(): ResponseEntity<Any> =
            ResponseEntity
                    .status(HttpStatus.MOVED_PERMANENTLY)
                    .header(HttpHeaders.LOCATION, "/messages")
                    .build()
}